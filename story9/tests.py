from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import time

# Create your tests here.
class UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'index.html')

    def test_correct_views_used(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, index)


class FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome("./chromedriver",chrome_options=chrome_options)
        self.selenium.get('http://127.0.0.1:8000/story9/')

    def tearDown(self):
        self.selenium.quit()

    def test_correct_page(self):
        selenium = self.selenium
        time.sleep(5)

        self.assertIn("Login", selenium.page_source)