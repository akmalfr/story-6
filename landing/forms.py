from django import forms
from .models import Status

class CreateStatus(forms.ModelForm):
    class Meta:
        model = Status
        fields = ['status']
        labels = {'status':'Status'}
        required = {'status':True}
        widgets = {
            'status':forms.Textarea(
                attrs = {
                    'id':'status-input'
                }
            )
        }

        