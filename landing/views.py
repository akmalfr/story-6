from django.shortcuts import render
from .models import Status
from .forms import CreateStatus
from datetime import datetime

# Create your views here.
def landing(request):
    if request.method == "POST":
        form = CreateStatus(request.POST)
        if form.is_valid():
            form.save()
    
    stats = Status.objects.all()
    form = CreateStatus()
    context = {
        'status': stats,
        'form': form,
    }
    return render(request, 'landing.html', context)