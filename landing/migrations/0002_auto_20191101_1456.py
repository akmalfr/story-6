# Generated by Django 2.2.6 on 2019-11-01 07:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landing', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='status',
            old_name='date',
            new_name='time',
        ),
        migrations.AlterField(
            model_name='status',
            name='status',
            field=models.TextField(max_length=300),
        ),
    ]
