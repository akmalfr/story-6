from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import landing
from .models import Status
from .forms import CreateStatus
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.

class UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('//')
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        response = Client().get('//')
        self.assertTemplateUsed(response, 'landing.html')\
    
    def test_model_create_status(self):
        Status.objects.create(status='Coba coba')
        count = Status.objects.all().count()
        self.assertEqual(count, 1)

    def test_form_valid(self):
        data = {'status':"Coba coba", 'time':datetime.now()}
        form = CreateStatus(data=data)
        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data['status'],"Coba coba")

class FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome("./chromedriver",chrome_options=chrome_options)
        self.selenium.get('http://127.0.0.1:8000')

    def tearDown(self):
        self.selenium.quit()

    def test_correct_page(self):
        selenium = self.selenium
        time.sleep(5)

        self.assertIn("Halo", selenium.page_source)

    def test_status_input(self):
        selenium = self.selenium
        time.sleep(5)

        status = selenium.find_element_by_id("status-input")
        submit = selenium.find_element_by_id("submit")

        stat = "Coba coba"
        status.send_keys(stat)
        time.sleep(5)
        submit.send_keys(Keys.RETURN)
        self.assertIn("Coba coba", selenium.page_source)

