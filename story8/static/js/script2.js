$(document).ready(() => {

    $('button').click(function(){
        var key = $('#search').val().toLowerCase();
        location.href = '#page2';
        
        $.ajax({
            method: 'GET',
            url: 'https://www.googleapis.com/books/v1/volumes?q=' + key,
            success: function(response){
                $('#books').empty()
                for(let i = 0; i < response.items.length; i++){
                    $('#books').append(
                    "<tr> <th scope='row'>" + (i + 1) + "</th>" +
                    "<td><img src='" + response.items[i].volumeInfo.imageLinks.smallThumbnail + "'></img>" + "</td>" +
                    "<td>" + response.items[i].volumeInfo.title + "</td>" +
                    "<td>" + response.items[i].volumeInfo.authors + "</td>" +
                    "<td>" + response.items[i].volumeInfo.publisher + "</td>" +
                    "<td>" + response.items[i].volumeInfo.publishedDate + "</td>" +
                    "<td>" + response.items[i].volumeInfo.description + "</td>"
                    );
                }
            }
        })
    })
})