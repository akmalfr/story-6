from django.urls import path
from .views import index

app_name = 'story8'

urlpatterns = [
    path('', index, name="index"),
]