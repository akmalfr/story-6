from django.urls import path
from .views import accordion

app_name = 'story7'

urlpatterns = [
    path('', accordion, name="accordion"),
]