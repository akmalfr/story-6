from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import accordion
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import time

# Create your tests here.
class UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'accordion.html')

    def test_correct_views_used(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, accordion)


class FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome("./chromedriver",chrome_options=chrome_options)
        self.selenium.get('http://127.0.0.1:8000/story7/')

    def tearDown(self):
        self.selenium.quit()

    def test_correct_page(self):
        selenium = self.selenium
        time.sleep(5)

        self.assertIn("Change Theme", selenium.page_source)

    def test_js_functions(self):
        selenium = self.selenium
        time.sleep(5)

        theme = self.selenium.find_element_by_css_selector('#theme-switch')

        self.selenium.execute_script('arguments[0].click();', theme)
        time.sleep(1)
        background_color = selenium.find_element_by_tag_name('body').value_of_css_property('background-color')
        self.assertIn("rgba(0, 0, 0, 1)", background_color)
        time.sleep(1)

        self.selenium.execute_script('arguments[0].click();', theme)
        time.sleep(1)
        background_color = selenium.find_element_by_tag_name('body').value_of_css_property('background-color')
        self.assertIn("rgba(255, 255, 255, 1)", background_color)
        time.sleep(1)

        list1 = ["Belajar", "None", "None"]
        list2 = ["0", "1", "2"]
        for i in range(0,3):
            accordion = selenium.find_element_by_id(list2[i])
            accordion.click()
            time.sleep(1)
            self.assertIn(list1[i], selenium.page_source)
            time.sleep(1)

        time.sleep(1)


    