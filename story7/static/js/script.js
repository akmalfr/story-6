const themeSwitch = $("#theme-switch");

$(function() {
    $( "#accordion" ).accordion({
        collapsible: true,
        active: false,
    });
} );

themeSwitch.click(function(){
    if (themeSwitch.is(":checked")) {
        $("body").css({"background-color": "black", "color": "white"});
        $("h3").css({"border-color": "white"});
        $(".accordion").css({"background-color": "#263238"});
        $("h1").css({"border-color": "white"});
    } else {
        $("body").css({"background-color": "white", "color": "black"});
        $("h3").css({"border-color": "black"});
        $(".accordion").css({"background-color": "#eceff1"});
        $("h1").css({"border-color": "black"});
    }
});